var express = require('express');
var pg = require("pg");
var app = express();

//var connectionString = "postgres://ysxufhrl:x4_v2y44hqqLQxnMwLpG7h9MIluFIFG_@packy.db.elephantsql.com:5432/ysxufhrl";

const { Pool, Client } = require("pg");
const connectionString = 'postgres://ysxufhrl:x4_v2y44hqqLQxnMwLpG7h9MIluFIFG_@packy.db.elephantsql.com:5432/ysxufhrl';
/*const pool = new Pool({
    connectionString: connectionString,
});
pool.query('SELECT NOW()', (err, res) => {
    console.log(err, res);
    pool.end()
});*/
const client = new Client({
    connectionString: connectionString,
});

app.get('/', function (req, res, next) {
    /*client.connect();
    client.query('SELECT * FROM public."event"', (err, res) => {
        if(err){
            console.log("not able to get connection "+ err);
        } else {
            console.log(res);
        }
        client.end()
    });
*/
    pg.connect(connectionString,function(err,client,done) {
        if(err){
            console.log("not able to get connection "+ err);
            res.status(400).send(err);
        }
        client.query("SELECT * FROM public.event" ,function(err,result) {
            done(); // closing the connection;
            if(err){
                console.log(err);
                res.status(400).send(err);
            } else {
                res.status(200).send(result.rows);
            }
        });
    });
});

app.listen(4000, function () {
    console.log('Server is running.. on Port 4000');
});