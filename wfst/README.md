# Демо веб-приложения

[Ссылка на sdk](https://github.com/planetfederal/sdk)

Для запуска необходимо

1. [Скачать](http://geoserver.org/) и установить геосервер

2. Запустить `start.sh/start.bat`

3. [Скачать](https://nodejs.org/en/) и установить **Node.js**

4. [Скачать](https://drive.google.com/file/d/1oCl1tEN7K13coEzoqegAvgcxtLH6OqaT/view?usp=sharing) файлы с шейпами и добавить их на геосервер

5. Открыть терминал, сменить директория на `.../web-demo/wfst`

6. Выполнить команду: `node node_modules/npm-run-all/lib/command.js createdir start:debug`

7. Выполнить команду: `node node_modules/json-http-proxy/bin/json-http-proxy -p 3978`

8. [Открыть](http://localhost:3978/) в браузере приложение
