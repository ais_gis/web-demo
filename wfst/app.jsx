import React from 'react';
import ReactDOM from 'react-dom';
import ol from 'openlayers';
import {addLocaleData, IntlProvider, defineMessages, injectIntl, intlShape} from 'react-intl';
import MapPanel from '@boundlessgeo/sdk/components/MapPanel';
import MapConfig from '@boundlessgeo/sdk/components/MapConfig';
import Header from '@boundlessgeo/sdk/components/Header';
import Measure from '@boundlessgeo/sdk/components/Measure';
import ImageExport from '@boundlessgeo/sdk/components/ImageExport';
import Navigation from '@boundlessgeo/sdk/components/Navigation';
import Zoom from '@boundlessgeo/sdk/components/Zoom';
import EditPopup from '@boundlessgeo/sdk/components/EditPopup';
import DrawFeature from '@boundlessgeo/sdk/components/DrawFeature';
import LoadingPanel from '@boundlessgeo/sdk/components/LoadingPanel';
import CustomTheme from './theme';
import FlatButton from 'material-ui/FlatButton';
import LeftNav from '@boundlessgeo/sdk/components/LeftNav';




import FeatureTable from '@boundlessgeo/sdk/components/FeatureTable';
import enLocaleData from 'react-intl/locale-data/ru';
import enMessages from '@boundlessgeo/sdk/locale/en';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
import QGISPrint from '@boundlessgeo/sdk/components/QGISPrint';
import HomeButton from '@boundlessgeo/sdk/components/HomeButton';
import Geolocation from '@boundlessgeo/sdk/components/Geolocation';

import {Tab} from 'material-ui/Tabs';

import Globe from '@boundlessgeo/sdk/components/Globe';
import LayerList from '@boundlessgeo/sdk/components/LayerList';
import Chart from '@boundlessgeo/sdk/components/Chart';

import Geocoding from '@boundlessgeo/sdk/components/Geocoding';
import GeocodingResults from '@boundlessgeo/sdk/components/GeocodingResults';
import Select from '@boundlessgeo/sdk/components/Select';
import QueryBuilder from '@boundlessgeo/sdk/components/QueryBuilder';
import Button from '@boundlessgeo/sdk/components/Button';
import InfoPopup from '@boundlessgeo/sdk/components/InfoPopup';



injectTapEventPlugin();


addLocaleData(
  enLocaleData
);



var vectorSource2 = new ol.source.Vector({
  format: new ol.format.GeoJSON(),

  url: function(extent, resolution, projection) {
    return '/geoserver/wfs?service=WFS&' +
        'version=1.1.0&request=GetFeature&typename=cite:dist2&' +
        'outputFormat=application/json&srsname=EPSG:3857&' +
        'bbox=' + extent.join(',') + ',EPSG:3857';
  },
  strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
    maxZoom: 19
  }))
});



var vector2 = new ol.layer.Vector({
  source: vectorSource2,
  isSelectable: true,

  id: 'wfst',
  wfsInfo: {
    featureNS: 'http://www.opengeospatial.net/cite',
    attributes: ['NAME', 'AREA', 'Shape_Leng'],
    featureType: 'dist2',
    featurePrefix: 'cite',
    geometryType: 'MultiPolygon',
    geometryName: 'the_geom',
    url: '/geoserver/wfs'
  },
  isWFST: true,
  title: 'Районы РБ',
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'black',
      width: 1
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255,102,255,0.5) '
    })
  })
});

var map = new ol.Map({
  controls: [],
  layers: [
    new ol.layer.Group({
      type: 'base-group',
      title: 'Базовые слои',
      layers: [
        new ol.layer.Tile({
          type: 'base',
          title: 'OpenStreetMap',
          source: new ol.source.OSM()
        }),
        new ol.layer.Tile({
          type: 'base',
          title: 'Yandex',
          visible: false,
          source: new ol.source.XYZ({
            url: 'http://vec02.maps.yandex.net/tiles?l=map&v=4.55.2&z={z}&x={x}&y={y}&scale=2&lang=ru_RU'
          })
        }),
        new ol.layer.Tile({
          type: 'base',
          title: 'Кадастровая карта',
          visible: false,
          source: new ol.source.XYZ({
            url: 'https://pkk5.rosreestr.ru/arcgis/rest/services/BaseMaps/BaseMap/MapServer/tile/{z}/{y}/{x}'
          })
        }),
        new ol.layer.Tile({
          type: 'base',
          title: 'Aerial',
          visible: false,
          source: new ol.source.XYZ({
            attributions: [
              new ol.Attribution({
                html: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
              })
            ],
            url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
          })
        })
      ]
    }),
    vector2
  ],
  view: new ol.View({
    center: ol.proj.fromLonLat([56, 54]),
    zoom: 7
  })
});

const style = new ol.style.Style({fill: new ol.style.Fill({color: 'orange'}), stroke: new ol.style.Stroke({color: 'black', width: 1})});
var printLayouts = [{
  name: 'Layout 1',
  thumbnail: 'layout1_thumbnail.png',
  width: 620.0,
  elements: [{
    name: 'Title',
    height: 40.825440467359044,
    width: 51.98353115727002,
    y: 39.25222551928783,
    x: 221.77507418397624,
    font: 'Helvetica',
    type: 'label',
    id: '24160ce7-34a3-4f25-a077-8910e4889681',
    size: 18
  }, {
    height: 167.0,
    width: 171.0,
    grid: {
      intervalX: 0.0,
      intervalY: 0.0,
      annotationEnabled: false,
      crs: ''
    },
    y: 19.0,
    x: 16.0,
    type: 'map',
    id: '3d532cb9-0eca-4e50-9f0a-ce29b1c7f5a6'
  }],
  height: 500.0
}
];


const messages = defineMessages({
  geocodingtab: {
    id: 'app.geocodingtab',
    description: 'Название',
    defaultMessage: 'Гео'
  },
  attributestab: {
    id: 'app.attributestab',
    description: 'Таблица атрибутов',
    defaultMessage: 'Таблица атрибутов'
  },
  querytab: {
    id: 'app.querytab',
    description: 'Title of the query tab',
    defaultMessage: 'Query'
  },
  charttab: {
    id: 'app.charttab',
    description: 'Title of the chart tab',
    defaultMessage: 'Charts'
  },
  chart1: {
    id: 'app.chart1',
    description: 'Title of the first chart',
    defaultMessage: 'Airports count per use category'
  },
  chart2: {
    id: 'app.chart2',
    description: 'Title of the second chart',
    defaultMessage: 'Forest area total surface'
  }
});


enMessages['app.geocodingtab'] = 'Найти место';
enMessages['app.attributestab'] = 'Таблица';
enMessages['app.querytab'] = 'Запросы';
enMessages['app.charttab'] = 'График';
enMessages['app.chart1'] = 'Number of airports per usage category';
enMessages['app.chart2'] = 'Total area of forest';

class WFSTApp extends React.Component {
  constructor(props, context) {
    super(props);
    this.state = {
      value: 1,
      leftNavOpen: false
    };
  }
  getChildContext() {
    return {
      muiTheme: getMuiTheme()
    };
  }
    _toggle(el) {
    if (el.style.display === 'block') {
      el.style.display = 'none';
    } else {
      el.style.display = 'block';
    }
  }
  _toggleTable() {
    this._toggle(ReactDOM.findDOMNode(this.refs.tablePanel));
    this.refs.table.getWrappedInstance().setDimensionsOnState();
  }

  handleChange(value) {
    if (value === parseInt(value, 10)) {
      this.setState({
        value: value
      });
    }
  }
  layerListOpen(value) {
    this.setState({
      addLayerOpen: true
    });
  }
  layerListClose(value) {
    this.setState({
      addLayerOpen: false
    });
  }
  leftNavOpen(value) {
    this.setState({
      leftNavOpen: true
    }, function() {
      map.updateSize();
    });
  }
  leftNavClose(value) {
    this.setState({
      leftNavOpen: false
    }, function() {
      map.updateSize();
    });
  }
  render() {
    const {formatMessage} = this.props.intl;

    var leftNavWidth = 360;
    var tabList = [
      <Tab key={1} value={1} label='Слои' onActive={this.layerListOpen.bind(this)}>
        <div id='layer-list'>
          <LayerList
              inlineDialogs={true}
              icon={<FlatButton label="Добавить"/>}
              addLayer={{
                open:this.state.addLayerOpen,
                onRequestClose:this.layerListClose.bind(this),
                allowUserInput: true,
                sources: [{url: '/geoserver/wms', type: 'WMS', title: 'Local GeoServer'}]}}
              allowFiltering={true}
              showOpacity={true}
              showDownload={true}
              showGroupContent={true}
              showZoomTo={true}
              allowReordering={true}
              map={map} />
        </div>
      </Tab>,
      <Tab key={2} value={2} label={formatMessage(messages.geocodingtab)}><div style={{background: CustomTheme.palette.canvasColor}} id='geocoding-tab'><Geocoding /></div><div id='geocoding-results' className='geocoding-results'><GeocodingResults map={map} /></div></Tab>,
      <Tab key={3} value={3} label={formatMessage(messages.attributestab)}><div id="attributes-table-tab" style={{height: '100%'}}><FeatureTable ref='table' map={map} /></div></Tab>,
    ];

    return (
      <div id='content'>
        <Header title='АИС АПК'
                showLeftIcon={!this.state.leftNavOpen}
                style={{left: this.state.leftNavOpen ? leftNavWidth : 0, width: this.state.leftNavOpen ? 'calc(100% - ' + leftNavWidth + 'px)' : '100%'}}
                onLeftIconTouchTap={this.leftNavOpen.bind(this)}>
          <Navigation toggleGroup='nav' secondary={true} />
          <DrawFeature toggleGroup='nav' map={map} />
          <Select toggleGroup='nav' map={map}/>
          <Measure toggleGroup='nav' map={map}/>
          <QGISPrint map={map} layouts={printLayouts} />
          <ImageExport map={map} />
          <Button toggleGroup='nav' buttonType='Icon' iconClassName='headerIcons ms ms-table' tooltip='Table' onTouchTap={this._toggleTable.bind(this)}/>


        </Header>
        <LeftNav tabList={tabList} open={this.state.leftNavOpen} onRequestClose={this.leftNavClose.bind(this)}/>
        <div className='map' style={{left: this.state.leftNavOpen ? leftNavWidth : 0, width: this.state.leftNavOpen ? 'calc(100% - ' + leftNavWidth + 'px)' : '100%'}}>

        <MapPanel id='map' map={map} />
        <LoadingPanel map={map} />

        <div id='home-button'><HomeButton map={map} /></div>
        <div id='zoom-buttons'><Zoom map={map} /></div>
        <div id='layerlist'><LayerList allowFiltering={true} showOpacity={true} showDownload={true} showGroupContent={true} showZoomTo={true} allowReordering={true} map={map} /></div>
        <div id='editpopup' className='ol-popup'><EditPopup toggleGroup='navigation' map={map} /></div>
        <div id='popup' className='ol-popup'><InfoPopup toggleGroup='navigation' map={map} /></div>
        <div ref='tablePanel' id='table-panel' className='attributes-table'><FeatureTable toggleGroup='navigation' ref='table' map={map} /></div>
        <div id='geolocation-control'><Geolocation map={map} /></div>
        </div>


      </div>
    );
  }
}
WFSTApp.propTypes = {
  /**
   * i18n message strings. Provided through the application through context.
   */
  intl: intlShape.isRequired
};
WFSTApp.childContextTypes = {
  muiTheme: React.PropTypes.object
};
WFSTApp = injectIntl(WFSTApp);


ReactDOM.render(<IntlProvider locale='en' messages={enMessages}><WFSTApp /></IntlProvider>, document.getElementById('main'));
